export function arrayToObjectById(array, keyName = 'id') {
  const result = {};
  for (let i = 0; i < array.length; i +=1) {
    const el = array[i];
    result[el[keyName]] = array[i];
  }
  return result;
}

export function stringToBase64(string) {
  return new Buffer(string).toString('base64');
}

export function htmlToSlackMarkup(string) {
  string = string.replace(new RegExp('<em>', 'g'), '*');
  string = string.replace(new RegExp('</em>', 'g'), '*');
  return string;
}

export function getIssueKeyFromESHighlightKey(highlightKey) {
  var indices = [];
  for(var i=0; i<highlightKey.length;i++) {
    if (highlightKey[i] === ".") indices.push(i);
  }
  return highlightKey.substring(indices[0] + 1, indices[1]);
}
