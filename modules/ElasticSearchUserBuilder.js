import JIRAService from '../services/JIRAService';
import ElasticSearchService from '../services/ElasticSearchService';
import * as utils from '../common/utils';

const jiraService = new JIRAService();
const esService = new ElasticSearchService();

/**
* @param {email} string user email address for JIRA account
* @param {slackUserId} string user ID in slack (on the message.user property from botkit responses)
* @param {userIssues} object describes a set of issues (JIRA issues, or user-defined "skills" that will be indexed with this user)
* Note: userIssues should have a property `issues: Array<object>`, where each object has at least a name, summary, and id property
*/
export default class ElasticSearchUserBuilder {

  async buildUserForIndexing(email, slackUserId, userIssues) {
    return new Promise(async (resolve, reject) => {
      try {
        /**
        * For user issues in the ES index, we create an associative array of issues by id, so we can O(1)
        * update issues for a user, and we dont wind up with duplicate issues in the user's history
        */
        const userIssuesById = utils.arrayToObjectById(userIssues.issues);
        const user = await esService.getUserByEmail(email);
        user.issueHistory = Object.assign({}, user.issueHistory, userIssuesById);
        user.slackUserId = slackUserId;
        resolve(user);
      } catch (err) {
        reject(err);
      }
    });
  }
}
