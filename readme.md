## Rubberducky helps you plan your day, and find helpful colleagues

## Features
- You can ask Rubberducky about help on a particular project, or with a more general skill or a tool.
- Rubberducky will refer you to colleagues who have a history of experience working on a task like yours.
- Rubberducky will ask you about your current work, so it can learn about what you do and what you know.

### Conversations/features:

- Settings menu https://github.com/panderasystems/rubberducky/issues/2
	- Enter username & password for JIRA

- User config conversation (first time user messages the bot) https://github.com/panderasystems/rubberducky/issues/1

		Hey boss!  Which of these email addresses belongs to you?
		(Buttons)
		dean.m.larbi@gmail.com >
		dom@traena.io >
		...
		etc (list retrieved from JIRA organization)


- "standup" - proceeds with the daily standup (should also execute on a timer) https://github.com/panderasystems/rubberducky/issues/9

	- Bot:		

				Here are the tasks I have found you're assigned to.

				Use mongodb to store data about Righttrack project
				Status
				In Progress

				TODO      IN PROGRESS      COMPLETE
				...
				Another task
				...
				FINISHED >


	- User:

			Clicks a transition (i.e. 'Complete')

	- Bot:

			Repeats standup prompt (with tasks transitioned)    
      https://github.com/panderasystems/rubberducky/issues/3

	- User:

		 	Clicks 'FINISHED >'
      https://github.com/panderasystems/rubberducky/issues/12
	- Bot:

			Thanks for your answers!

- Standup report: https://github.com/panderasystems/rubberducky/issues/7
 	- Bot:

 				Hey team!  Here are the results from today's standup
				@dlarbi
				Tasks transitioned:
				https://rubberducky.atlassian.net/browse/SE-2
				In Progress
				https://rubberducky.atlassian.net/browse/SE-2
				Complete
				...
				Other users tasks
				...


- "find help <task,skill,or project>" - will refer you to a user with a history of working on your task
	- Bot:

				You got it boss!  Here are some people with skills you might find interesting.
				Here's someone with related skills!
				User
				@dlarbi
				Relevancy 34%
				History with this skill >
	- User:

    			Clicks 'History with this skill'
	- Bot:

				Here are some times @dlarbi has completed tasks or added skills related to your search.
				https://rubberducky.atlassian.net/browse/SE-2
				Add notifications to the *iOS* and Android applications for comments and community mentions

- "add a skill" - allows you to manually add skills to your user profile, so other users can find you
