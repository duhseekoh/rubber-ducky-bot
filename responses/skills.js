import { JIRA_HOST_URL } from '../constants/constants';
import * as utils from '../common/utils';

/**
* Transforms response from ES query to search for users with a particular skill
* into a slack response that shows the best matches for the searched skill
* @param {skillsSearchResult} string
*/
export function buildSkillSearchResponse(skillsSearchResult) {
  const topResult = skillsSearchResult[0]._source;
  const score = skillsSearchResult[0]._score;
  const highlight = skillsSearchResult[0].highlight;
  const response = {
      "text": "Here's someone with related skills!",
      "attachments": [
        {
          "title": "User",
          "text": `<@${topResult.slackUserId}>`
        },
        {
          "title": "Relevancy",
          "text": `${(score * 100).toFixed(0)}%`
        },
        {
          "title": "History with this skill",
          "callback_id": 'skillResponse',
          "actions": [{
             "name": 'skillDetails',
             "text": `${Object.keys(highlight).length} Mention${Object.keys(highlight).length > 1 ? 's' : ''} of this skill`,
             "type": "button",
             "style": 'primary',
             "value": 'skillDetails'
          }]
        },
      ]
  };
  return response;
}

export function buildSkillDetailsResponse(skillsSearchResult) {
  const highlight = skillsSearchResult[0].highlight;
  const response = {
      "text": `Here are some times <@${skillsSearchResult[0]._source.slackUserId}> has completed tasks or added skills related to your search.`,
      "attachments": Object.keys(highlight).map(key => {
        const issueKey = utils.getIssueKeyFromESHighlightKey(key);
        const exampleTitle = skillsSearchResult[0]._source.issueHistory[issueKey].key ?
          `https://${JIRA_HOST_URL}/browse/${skillsSearchResult[0]._source.issueHistory[issueKey].key}` :
          `Added via 'add a skill'`;
        return {
          "color": "#00a9e5",
          "title": exampleTitle,
          "text": utils.htmlToSlackMarkup(highlight[key].join(' ')),
          "mrkdwn_in": ["text"]
        }
      })
  };
  return response;
}
