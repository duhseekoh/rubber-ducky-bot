import { JIRA_HOST_URL } from '../constants/constants';

/**
* Transforms response from JIRA API into slack response about what issues a user
* currently has assigned to them
* @param {userIssues} object <JIRA API userIssues response>
*/
export function buildStandupResponse(userIssues, transitions) {
  return {
    text: 'Here are the tasks I have found you\'re assigned to.',
    attachments: userIssues.issues.map(issue => {
      return {
          "callback_id": issue.id,
          "fallback": issue.fields.summary,
          "color": "#36a64f",
          "title": issue.fields.summary,
          "title_link": `https://${JIRA_HOST_URL}/browse/${issue.key}`,
          "fields": [
              {
                  "title": "Status",
                  "value": issue.fields.status.name,
                  "short": false
              }
          ],
          "footer": "JIRA API",
          "actions": transitions[issue.id].map(transition => ({
             "name": transition.name,
             "text": transition.name,
             "type": "button",
             "style": issue.fields.status.name == transition.name ? "primary" : "default",
             "value": transition.id
           })),
      }
    })
  }
}
