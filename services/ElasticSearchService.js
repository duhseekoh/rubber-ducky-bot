import elasticsearch from 'elasticsearch';
import * as CONSTANTS from '../constants/constants';
import * as utils from '../common/utils';

export default class ElasticSearchService {

  constructor() {
    this.esClient = new elasticsearch.Client({
      host: CONSTANTS.ELASTICSEARCH_HOST_URL,
      log: 'trace'
    });
    return this;
  }

  async createIndex(indexName) {
    return new Promise((resolve, reject) => {
      this.esClient.indices.create({
        index: indexName
      }, (err, response) => {
        if (err) {
          reject(err);
        }
        resolve(response);
      });
    });
  }

  async getIndexExists(indexName) {
    return new Promise((resolve, reject) => {
      this.esClient.indices.exists({
        index: indexName,
      }, (err, exists) => {
        if (err) {
          reject(err);
        }
        resolve(exists);
      });
    });
  }

  async searchSkill(skillToSearch) {
    return new Promise((resolve, reject) => {
      this.esClient.search({
        index: 'rubberducky',
        body: {
          query: {
            query_string: {
              query: skillToSearch,
              // fields: ['issueHistory'],
            }
          },
          highlight: {
            fields: {
              "*": {}
            },
            require_field_match: false
          }

        }
      }, (err, response) => {
        if (err) {
          reject(err);
        }
        resolve(response.hits.hits);
      });
    });
  }

  async getAllUsers() {
    return new Promise((resolve, reject) => {
      this.esClient.search({
        index: 'rubberducky',
        body: {
          query: {
            match: {}
          },
        }
      }, (err, response) => {
        if (err) {
          reject(err);
        }
        resolve(response.hits.hits);
      });
    })
  }

  async getUserByEmail(email) {
    return new Promise((resolve, reject) => {
      this.esClient.search({
        index: 'rubberducky',
        body: {
          query: {
            ids: {
              values: [email]
            }
          },
        }
      }, (err, response) => {
        if (err) {
          reject(err);
        }
        const user = response.hits.hits.length ? response.hits.hits[0]._source : { email: email, issueHistory: {} };
        resolve(user);
      });
    })
  }

  async upsertUserAndSkillsData(user) {
    const indexUserQuery = {
      index: 'rubberducky',
      type: 'user',
      id: user.email,
      body: user
    };
    console.log('Indexing user', user)
    return new Promise((resolve, reject) => {
      this.esClient.index(indexUserQuery, (err, response) => {
        if (err) {
          reject(err);
        }
        resolve(response);
      })
    });
  }
}
