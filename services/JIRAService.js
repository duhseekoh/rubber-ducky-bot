import { JiraApi } from 'jira';
import * as CONSTANTS from '../constants/constants';

export default class JIRAService {

  constructor(jiraClient) {
    this.jiraClient = new JiraApi(
      'https',
      CONSTANTS.JIRA_HOST_URL,
      443,
      process.env.jiraUser,
      process.env.jiraPassword,
      '2'
    );
    return this;
  }

  async getUser(id) {

  }

  async getUserIssues(email) {
    return new Promise((resolve, reject) => {
      this.jiraClient.getUsersIssues(email, false, (err, result) => {
        if (err) {
          console.log(err)
          reject(err);
        }
        console.log(email, err, result)
        resolve(result);
      });
    });
  }

  /**
  * Returns list of transitions available for one issue
  * @param {issueId} number the JIRA ID of the issue for which you want to retrieve available transitions
  * Array[{ id: '21',
  *  name: 'In Progress',
  *  to:
  *   { self: 'https://rubberducky.atlassian.net/rest/api/2/status/3',
  *     description: 'This issue is being actively worked on at the moment by the assignee.',
  *     iconUrl: 'https://rubberducky.atlassian.net/images/icons/statuses/inprogress.png',
  *     name: 'In Progress',
  *     id: '3',
  *     statusCategory: [Object] },
  *     hasScreen: false,
  *     fields: {}
  *  }]
  */
  async getIssueTransitions(issueId) {
    return new Promise((resolve, reject) => {
      this.jiraClient.listTransitions(issueId, (err, result) => {
        if (err) {
          reject(err);
        }
        resolve(result.transitions);
      });
    });
  }

  /**
  * @param {issues} Array Array of JIRA issue objects, each issue should have id property (JIRA Issue ID)
  * Returns list of transitions available for a set of issues, with issueIds as keys
  * @returns Object {<issueId>: { id: '21',
  *  name: 'In Progress',
  *  to:
  *   { self: 'https://rubberducky.atlassian.net/rest/api/2/status/3',
  *     description: 'This issue is being actively worked on at the moment by the assignee.',
  *     iconUrl: 'https://rubberducky.atlassian.net/images/icons/statuses/inprogress.png',
  *     name: 'In Progress',
  *     id: '3',
  *     statusCategory: [Object] },
  *     hasScreen: false,
  *     fields: {}
  *   }
  *  }
  */
  async getIssuesTransitionsByIssueId(issues) {
    const result = {};
    for (let i = 0; i < issues.length; i +=1) {
      const issue = issues[i];
      const transitions = await this.getIssueTransitions(issue.id);
      result[issue.id] = transitions;
    }
    return result;
  }

  async updateIssue(issueId, transitionId) {
    return new Promise(async (resolve, reject) => {
      const transitions = await this.getIssueTransitions(issueId);
      this.jiraClient.transitionIssue(issueId, {
        transition: {
          id: transitionId
        }
      }, (err, result) => {
        if (err) {
          reject(err);
        }
        resolve(result);
      });
    });
  }
}
