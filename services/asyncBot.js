/*
* Here we promisify various bot behaviors, so we can alleviate callbacks in our code
*/
export const startConversation = (bot, message) => {
  return new Promise((resolve, reject) => {
    bot.startConversation(message, async (err, convo) => {
      if (err) {
        reject(err);
      }
      resolve(convo);
    });
  })
}
