import * as responses from '../responses/skills';
import * as utils from '../common/utils';
import ElasticSearchService from '../services/ElasticSearchService';
import ElasticSearchUserBuilder from '../modules/ElasticSearchUserBuilder';

const esService = new ElasticSearchService();

module.exports = function(controller) {

    controller.hears(['Add a skill', 'add a skill', 'add skill'], 'direct_message,direct_mention', function(bot, message) {
        bot.startConversation(message, function(err, convo) {
            convo.say('Ok!  When you add a skill, other people will be able to find you to help them out.');
            convo.ask('What skill would you like to add?', [
              {
                pattern:  '(.*)',
                callback: function(response, convo) {
                  const slackUserId = message.user;
                  const userIssues = {
                    issues: [
                      {
                        id: utils.stringToBase64(response.text),
                        fields: {
                          summary: response.text,
                        }
                      }
                    ]
                  }

                  convo.say(`Excellent.  I\'ll make it so people can find you for ${response.text}.`);
                  convo.ask(`I'd like some details about your ${response.text} skills.  Maybe you have a specialty in this area?  Anything you'd like to share with your colleagues is helpful!`, [
                    {
                      pattern:  '(.*)',
                      callback: async (response, convo) => {
                        convo.say(`Thanks for the details.`);
                        userIssues.issues[0].fields.description = response.text;
                        const user = new ElasticSearchUserBuilder();
                        const userForIndexing = await user.buildUserForIndexing('dean.m.larbi@gmail.com', slackUserId, userIssues);
                        const indexResult = await esService.upsertUserAndSkillsData(userForIndexing);
                        convo.next();
                      } // end second callback
                    }
                  ]);
                  convo.next();
                }
              },
            ]);

        });

    });

    controller.hears(['Find help (.*)', 'Find a help (.*)', 'find help (.*)', 'find a help (.*)'], 'direct_message,direct_mention', function(bot, message) {
      const skillToSearch = message.match[1];

      bot.startConversation(message, async (err, convo) => {
        const skillsSearchResult = await esService.searchSkill(skillToSearch);
        if (!skillsSearchResult.length) {
          convo.say('I didn\'t find anybody with the skill you asked about.')
          convo.next();
          return;
        }

        const skillSearchResponse = responses.buildSkillSearchResponse(skillsSearchResult);
        convo.say('You got it boss!  Here are some people with skills you might find interesting.');
        convo.next();
        convo.ask(skillSearchResponse, [
          {
            pattern:  '(.*)',
            callback: function(response, convo) {
              if (response.text != 'skillDetails') {
                convo.next();
                return;
              }
              const skillDetailsResponse = responses.buildSkillDetailsResponse(skillsSearchResult);
              convo.say(skillDetailsResponse);
              convo.next();
            }
          },
        ]);
        convo.next();
      });
    });
};
