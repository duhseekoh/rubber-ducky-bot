import JIRAService from '../services/JIRAService';
import * as asyncBot from '../services/asyncBot';
import ElasticSearchService from '../services/ElasticSearchService';
import ElasticSearchUserBuilder from '../modules/ElasticSearchUserBuilder';
import * as responses from '../responses/standup';
import * as semanticAnalysisService from '../services/SemanticAnalysisService';
import * as utils from '../common/utils';

const jiraService = new JIRAService();
const esService = new ElasticSearchService();

module.exports = (controller) => {
  controller.hears(['standup'], 'direct_message,direct_mention', async (bot, message) => {
    const convo = await asyncBot.startConversation(bot, message);

    const slackUserId = message.user;
    const userIssues = await jiraService.getUserIssues('dean.m.larbi@gmail.com');

    /**
    * We build a user and skills document and index it into ES,
    */
    const user = new ElasticSearchUserBuilder();
    const userForIndexing = await user.buildUserForIndexing('dean.m.larbi@gmail.com', slackUserId, userIssues);
    const indexResult = await esService.upsertUserAndSkillsData(userForIndexing);

    /**
    * We build a slack response with all the user's current JIRA issues
    */
    const transitions = await jiraService.getIssuesTransitionsByIssueId(userIssues.issues);
    const userIssuesResponse = responses.buildStandupResponse(userIssues, transitions);
    convo.ask(userIssuesResponse, [
      {
        pattern:  '(.*)',
        callback: async (response, convo) => {
          if (isNaN(response.text)) {
            convo.next();
            return;
          }
          const issueId = response.callback_id;
          const transitionId = response.text;
          try {
            await jiraService.updateIssue(issueId, transitionId);
          } catch (err) {
            console.log('Couldnt update issue: ', err);
          }
          convo.next();
        }
      }
    ]); // end ask userIssuesResponse
  });
}
