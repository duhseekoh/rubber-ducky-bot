var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'elastic:changeme@localhost:9200',
  log: 'trace'
});

module.exports = function(controller) {

    controller.hears(['Delete users'], 'direct_message,direct_mention', function(bot, message) {

        bot.startConversation(message, function(err, convo) {
            convo.say('Ok!  I am deleting everything');
            client.search({
              index: 'rubberducky',
              body: {
                query: {
                  match_all: {}
                },
              }
            }, function (error, response) {
              console.log('Results:', response.hits.hits);
              response.hits.hits.forEach((hit) => {
                client.delete({
                  index: 'rubberducky',
                  type: 'user',
                  id: hit._id
                }, function (error, response) {
                  console.log('Deleted', err, response);
                });
              })
            });

        });

    });

    /**
    * Deprecated:  Incorrect structure for data, incorrect type, etc
    */
    controller.hears(['Seed help'], 'direct_message,direct_mention', function(bot, message) {
        const projectHelp = [
          {
            index: 'rubberducky',
            type: 'project-help',
            body: {
              user: 'Dennis Janek',
              project: 'RAT Tool',
              role: 'MongoDB.  Database administration.  Documentation'
            }
          },
          {
            index: 'rubberducky',
            type: 'project-help',
            body: {
              user: 'Jeff',
              project: 'Traena',
              role: 'Project planning. JIRA. Documentation. Sharks'
            }
          },
          {
            index: 'rubberducky',
            type: 'project-help',
            body: {
              user: 'Ed Siok',
              project: 'Parallon',
              role: 'Architecture. Redux. React. Unidirectional data flow.'
            }
          },
          {
            index: 'rubberducky',
            type: 'project-help',
            body: {
              user: 'Dom Dicicco',
              project: 'Traena',
              role: 'React Native. Redux. Flow. Unidirectional data flow.'
            }
          },
          {
            index: 'rubberducky',
            type: 'project-help',
            body: {
              user: 'Ben Madore',
              project: 'RAT Tool',
              role: 'Java. Kotlin. RESTful webservices.'
            }
          }
        ]
        bot.startConversation(message, function(err, convo) {
            convo.say('Ok!  I am seeding project help');
            projectHelp.forEach((helpToIndex) => {
              client.index(helpToIndex, function (error, response) {
                console.log('Results:', response);
              });
            });
        });
    });


};
